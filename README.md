## About Task

- Laravel framework used as skeleton.
- All classes are available in `app/classes/` directory.

## Usage
- Clone project from `main` branch and install Laravel dependecies with `composer install`
- Put your CSV file in `public/input/` directory.
- Use this command for execution: `php artisan analyze {yourfile}`, Don't forget to use this command in the project root directory.
- There is a test file `test.csv` in the `public/input/` directory. You can call `php artisan analyze test.csv` in the project root directory to process file then you can see the output.  
[example output image](https://i.ibb.co/vJ9mnXR/Untitled.png)
