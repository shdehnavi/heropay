<?php

namespace App\Classes;

use function Symfony\Component\String\b;

class CommissionFee
{
    const DEPOSIT_FEE_PERCENTAGE = 0.0003;
    const WITHDRAW_BUSINESS_FEE_PERCENTAGE = 0.005;
    const WITHDRAW_PRIVATE_FEE_PERCENTAGE = 0.003;
    const WITHDRAW_PRIVATE_FREE_CHARGE_AMOUNT = 1000;
    const WITHDRAW_PRIVATE_FREE_CHARGE_TIMES = 3;

    private $csvFilePath;
    private $currencyExchangeRates;
    private $currencyDecimalPlaces;
    private $baseCurrency;
    private $operations;
    private $commissionData;

    public function __construct($csvFilePath) {
        $this->currencyExchangeRates = [
            'EUR' => [
                'JPY' => 129.53,
                'USD' => 1.1497,
            ]
        ];
        $this->currencyDecimalPlaces = [
            'EUR' => 2,
            'JPY' => 0,
            'USD' => 2,
        ];

        $this->baseCurrency = env('BASE_CURRENCY', 'EUR');
        $this->csvFilePath = $csvFilePath;
        $this->operations = $this->parseCsvFileToArray();
        $this->commissionData = [];
        $this->analyzeData();
    }

    private function parseCsvFileToArray() {
        return array_map('str_getcsv', file($this->csvFilePath));
    }

    public function analyzeData() {
        foreach ($this->operations as $operation) {
            $operationDate = $operation[0];
            $userId = $operation[1];
            $userType = $operation[2];
            $operationType = $operation[3];
            $operationAmount = $operation[4];
            $operationCurrency = $operation[5];
            $operationWeekId = $this->getWeekId($operationDate);

            $commissionFeeObject = new CommissionFeeObject();
            $commissionFeeObject->userId = $userId;
            $commissionFeeObject->userType = $userType;
            $commissionFeeObject->operationType = $operationType;
            $commissionFeeObject->operationWeekId = $this->getWeekId($operationDate);
            $commissionFeeObject->operationCurrency = $operationCurrency;
            $commissionFeeObject->operationAmount = $operationAmount;

            switch ($operationType) {
                case 'deposit': {
                    $commissionFeeObject->fee = Math::roundUp($operationAmount * self::DEPOSIT_FEE_PERCENTAGE, $this->currencyDecimalPlaces[$operationCurrency]);
                    $this->commissionData[] = $commissionFeeObject;
                    break;
                }

                case 'withdraw': {
                    switch ($userType) {
                        case 'private': {
                            $operationAmountInBaseCurrency = $this->convertCurrency($operationAmount, $operationCurrency, $this->baseCurrency);
                            $totalWeekAmount = 0;
                            $nbWithdraws = 0;
                            foreach ($this->commissionData as $commissionDatum) {
                                if ($commissionDatum->userId == $userId
                                    && $commissionDatum->userType == $userType
                                    && $commissionDatum->operationWeekId == $operationWeekId
                                    && $commissionDatum->operationType == $operationType
                                ) {
                                    if ($nbWithdraws < self::WITHDRAW_PRIVATE_FREE_CHARGE_TIMES) {
                                        $totalWeekAmount += $this->convertCurrency($commissionDatum->operationAmount, $commissionDatum->operationCurrency, $this->baseCurrency);;
                                    }
                                    $nbWithdraws ++;
                                }
                            }

                            if ($totalWeekAmount < self::WITHDRAW_PRIVATE_FREE_CHARGE_AMOUNT && $nbWithdraws < self::WITHDRAW_PRIVATE_FREE_CHARGE_TIMES) {
                                if ($operationAmountInBaseCurrency - (self::WITHDRAW_PRIVATE_FREE_CHARGE_AMOUNT - $totalWeekAmount) < 0) {
                                    $commissionFeeObject->fee = 0;
                                } else {
                                    $commissionFeeObject->fee = Math::roundUp($this->convertCurrency(($operationAmountInBaseCurrency - (self::WITHDRAW_PRIVATE_FREE_CHARGE_AMOUNT - $totalWeekAmount)) * self::WITHDRAW_PRIVATE_FEE_PERCENTAGE,$this->baseCurrency, $operationCurrency), $this->currencyDecimalPlaces[$operationCurrency]);
                                }
                            } else {
                                $commissionFeeObject->fee = Math::roundUp($this->convertCurrency($operationAmountInBaseCurrency * self::WITHDRAW_PRIVATE_FEE_PERCENTAGE, $this->baseCurrency, $operationCurrency), $this->currencyDecimalPlaces[$operationCurrency]);
                            }

                            $this->commissionData[] = $commissionFeeObject;
                            break;
                        }
                        case 'business': {
                            $commissionFeeObject->fee = Math::roundUp($operationAmount * self::WITHDRAW_BUSINESS_FEE_PERCENTAGE, $this->currencyDecimalPlaces[$operationCurrency]);
                            $this->commissionData[] = $commissionFeeObject;
                            break;
                        }
                    }

                    break;
                }
            }
        }
    }

    private function convertCurrency($amount, $fromCurrency, $toCurrency) {
        if ($fromCurrency == $toCurrency) {
            $convertedAmount = $amount;
        } elseif ($fromCurrency == $this->baseCurrency) {
            $convertedAmount = ($amount * floatval($this->currencyExchangeRates[$fromCurrency][$toCurrency]));
        } else {
            $convertedAmount = ($amount / floatval($this->currencyExchangeRates[$toCurrency][$fromCurrency]));
        }
        return $convertedAmount;
    }

    private function getWeekId($date) {
        $monthNumber = (int) date('m', strtotime($date));
        $weekNumber = (int) date('W', strtotime($date));
        $yearNumber = (int) date('Y', strtotime($date));

        if ($monthNumber == 12 && $weekNumber < 5) {
            $yearNumber += 1;
        } elseif ($monthNumber == 1 && $weekNumber > 5) {
            $yearNumber -= 1;
        }
        return $yearNumber . '-' . $weekNumber;
    }

    public function getRawCommissionFees() {
        $output = '';
        foreach ($this->commissionData as $commissionDatum) {
            $output .= $commissionDatum->fee . PHP_EOL;
        }

        return $output;
    }
}
