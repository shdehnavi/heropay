<?php

namespace App\Classes;

class CommissionFeeObject
{
    public $userId;
    public $userType;
    public $operationType;
    public $operationWeekId;
    public $operationCurrency;
    public $operationAmount;
    public $fee;
}
