<?php

namespace App\Classes;

class Math
{
    public static function roundUp ($value, $precision) {
        $pow = pow(10, $precision);
        return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
    }
}
