<?php

namespace App\Console\Commands;

use App\Classes\CommissionFee;
use Illuminate\Console\Command;

class AnalyzeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analyze {file=test.csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analyze CSV input file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filePath = public_path('/input/') . $this->argument('file');
        $commissionFee = new CommissionFee($filePath);
        echo $commissionFee->getRawCommissionFees();
    }
}
